﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(wcf_test.Startup))]
namespace wcf_test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
